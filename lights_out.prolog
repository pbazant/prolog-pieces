% This program solves the Lights Out puzzle. See
% https://en.wikipedia.org/wiki/Lights_Out_(game)

:- use_module(library(clpb)).
:- use_module(library(clpfd), [transpose/2]).

list_shifted_left_right(List, Shifted_left, Shifted_right) :-
	append(List, [0], [_|Shifted_left]),
	append(Shifted_right, [_], [0|List]).

matrix_shifted(Matrix, Shifted_left, Shifted_right, Shifted_up, Shifted_down) :-
	maplist(list_shifted_left_right, Matrix, Shifted_left, Shifted_right),
	transpose(Matrix, Matrix_t),
	maplist(list_shifted_left_right, Matrix_t, Shifted_up_t, Shifted_down_t),
	maplist(transpose, [Shifted_up_t, Shifted_down_t], [Shifted_up, Shifted_down]).

light_off(Initial_state, A0, A1, A2, A3, A4) :-
	sat(~(Initial_state # A0 # A1 # A2 # A3 # A4)).

solve_lights_out(Initial_matrix, Solution_matrix) :-
	maplist(same_length, Initial_matrix, Solution_matrix),
	matrix_shifted(Solution_matrix, L, R, U, D),
	maplist(maplist(light_off), Initial_matrix, Solution_matrix, L, R, U, D),
	append(Solution_matrix, Solution_variables),
	labeling(Solution_variables).

solve_example(Example, Solution) :-
	Example = [
	    [1,0,0,0,1],
	    [1,1,1,0,0],
	    [1,1,0,1,1],
	    [0,1,1,0,1],
	    [0,0,1,1,1]
	],
	solve_lights_out(Example, Solution).
