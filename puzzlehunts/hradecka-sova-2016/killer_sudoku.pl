﻿% This program solves the puzzle no. 8 of the "Hradecká sova 2016"
% outdoor day puzzlehunt.
% The puzzle is available at
% http://sova.osjak.cz/2016-data/sifry/08_sifra.pdf

% tested under SWI Prolog

:-use_module(library(clpfd)).

sudoku([
    [_,4,_,_,_,_,9,_,3],
    [_,_,_,_,1,_,_,_,_],
    [_,5,9,_,_,_,_,4,_],
    [_,_,7,_,_,_,1,_,2],
    [5,_,_,_,_,_,_,_,_],
    [_,_,8,_,_,_,_,_,_],
    [_,_,_,_,_,_,_,1,_],
    [_,_,_,7,_,_,_,_,_],
    [_,_,_,_,6,_,_,_,_]
]).

region_map([
    [a,a,a,b,c,d,d,d,e],
    [a,f,a,c,c,g,h,i,e],
    [f,f,j,j,g,g,h,i,i],
    [k,j,j,l,g,m,n,o,i],
    [k,k,p,l,m,m,n,o,o],
    [q,p,p,l,r,m,n,n,s],
    [q,t,t,u,u,v,s,s,s],
    [w,t,u,u,x,v,v,y,z],
    [w,w,zz,x,x,x,y,y,z]
]).

sums(
    [A, B, C, D, E, F, G] -
    [
        a-A, b-2, c-B, d-24, e-9,
        f-20, g-23, h-10, i-C,
        j-28,
        k-18, l-E, m-19, n-F, o-21,
        p-12,
        q-D, r-7, s-21,
        t-14, u-18, v-17,
        w-13, x-19, y-18, z-12,
        zz-G
    ]
).

sudoku_boxes(Three_rows, Boxes) :-
    transpose(Three_rows, [T1,T2,T3,T4,T5,T6,T7,T8,T9]),
    maplist(append, [[T1,T2,T3], [T4,T5,T6], [T7,T8,T9]], Boxes).

satisfies_sudoku(Rows) :-
    append(Rows, Vs),
    Vs ins 1..9,
    maplist(all_distinct, Rows),
    transpose(Rows, Columns),
    maplist(all_distinct, Columns),

    Rows = [R1, R2, R3, R4, R5, R6, R7, R8, R9],
    maplist(sudoku_boxes, [[R1,R2,R3], [R4,R5,R6], [R7,R8,R9]], Boxess),
    maplist(maplist(all_distinct), Boxess).


matrix_regions(Region_map, Matrix, Key_region_pairs) :-
    append(Region_map, Region_map_flat),
    append(Matrix, Matrix_flat),
    pairs_keys_values(
        Correspondence,
        Region_map_flat, Matrix_flat
    ),
    keysort(Correspondence, Correspondence_sorted),
    group_pairs_by_key(Correspondence_sorted, Key_region_pairs).

helper(Key_sum_pairs, Key-Region) :-
    member(Key-Sum, Key_sum_pairs),
    sum(Region, #=, Sum).

solve(Sudoku_rows, Region_map, Relevant_sums-Key_sum_pairs, Solution) :-
    satisfies_sudoku(Sudoku_rows),
    matrix_regions(Region_map, Sudoku_rows, Key_region_pairs),
    maplist(helper(Key_sum_pairs), Key_region_pairs),

    labeling([ff], Relevant_sums),

    atom_chars(abcdefghijklmnopqrstuvwxyz, Letters),
    findall(
        Letter,
        (member(A, Relevant_sums), nth1(A, Letters, Letter)),
        Solution
    ).


solution(Solution) :-
    sudoku(S), region_map(RM), sums(Sums), solve(S, RM, Sums, Solution).


% ?- solution(S).
% S = [p, o, l, i, j, m, e] ;
% false.

% "polij me" was the intermediate solution -- it means "pour on me" in
% Czech. Pouring water on the puzzle revealed yet another text (in
% Braille), which was the final solution.
