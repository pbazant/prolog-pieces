﻿% This program solves the puzzle no. 1 of the "Hradecká sova 2016"
% outdoor day puzzlehunt.
% The puzzle is available at
% http://sova.osjak.cz/2016-data/sifry/01_sifra.pdf

% tested under SWI Prolog

:-use_module(library(clpfd)).

solve_1([A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R]) :-
	[A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R] ins 0..9,
	A*10+B #= _*12,
	A*10+B+7 #= C*10+D,
	10*F+G #= 10*K+L+5,
	10*K+L #= 10*A+B+8,
	10*M+N #= J+K,
	10*O+P #= 10*M+N-1,
	10*Q+R #= 10*D+H+2,
	100*A+10*E+I #= 9*(10*K+L),
	10*D+H #= F+G,
	10*J+K #= 10*G+F,
	10*M+R #= A+E+I,
	label([A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R]).



solve_2([A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T]) :-
	[A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T] ins 0..9,
	10*A+B #= J+K,
	10*C+D #= 10*K+J,
	10*E+F #= 10*A+B + 11,
	10*J+K #= 10*S+T -11,
	M #= L+1,
	100*P+10*Q+R #= 100*C+10*H+L+6,
	S+T #= 13,
	1000*B+100*G+10*K+O #= 11*(10*S+T),
	100*C+10*H+L #= 4*(10*S+T),
	10*E+I #= 2*(10*A+B),
	Q #= N+1,
	label([A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T]).



solve_3([A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S]) :-
	[A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S] ins 0..9,
	10*A+B #= 10*A+F-6,
	10*C+D #= E+F,
	10*E+F #= 10*N+S + 10*G+H,
	10*G+H #= 10*E+I - (10*R+S),
	10*J+K #= 10*H+C,
	M #= L+1,
	10*N+O #= G+H,
	10*P+Q #= 10*R+S + 10*A+F,
	10*R+S #= G+H,
	10*A+F #= 10*N+S + 10*C+H,
	10*C+H #= 10*N+O + 2,
	10*E+I #= 8*(10*R+S),
	10*J+M #= 10*C+D + 10*A+B + 9,
	10*L+Q #= 10*R+S + 10*A+F,
	10*N+S #= G+H,
	label([A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S]).



solve_4([A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S]) :-
	[A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S] ins 0..9,
	B #= A+1,
	10*C+D #= 11*(I+M),
	10*E+F #= C+D,
	100*I+10*J+K #= 100*N+ 10*O+P + 11,
	O #= N+1,
	P #= O+1,
	10*Q+R #= 10*L+P + 10*E+F,
	10*A+F #= 10*O+S + 5,
	10*C+G #= 9*(10*E+H),
	10*E+H #= A+F,
	10*I+M #= C+D,
	10*K+N #= 10*E+F + 10*O+S,
	P #= L+1,
	S #= O+1,
	label([A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S]).





