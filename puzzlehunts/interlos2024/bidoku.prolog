% This program solves the puzzle "L9 Losí bidoku"
% that was part of the "InterLos 2024" online puzzle solving competition.
% The puzzle is available at
% https://interlos.fi.muni.cz/download/years/2024/sada3/sada3.pdf

:-use_module(library(clpfd)).

zadani([
    [_,_,_,_,_,_, _,_,_,_,_,_, _,_,_,_,_,_],
    [_,0,_,_,_,_, _,_,_,_,_,0, 1,1,1,1,_,_],

    [0,1,1,0,0,1, _,_,_,_,_,1, _,_,_,_,_,_],
    [_,1,_,_,_,0, 1,1,1,1,_,0, _,_,_,_,_,_],

    [_,1,_,_,_,0, _,_,_,_,_,0, _,1,_,_,_,_],
    [_,1,_,_,_,1, _,_,_,_,_,1, _,0,_,_,_,_],


    [0,_,_,_,0,1, 1,0,0,_,_,1, _,0,_,_,_,_],
    [1,_,_,1,_,0, 0,1,1,0,0,_, _,1,_,_,_,_],

    [1,0,_,0,_,0, _,_,0,1,1,0, 0,1,_,_,_,_],
    [0,1,1,0,0,1, 1,1,1,_,_,_, _,0,_,_,_,_],

    [0,1,0,1,1,0, 0,_,_,_,_,_, _,0,_,_,_,0],
    [_,0,_,1,_,0, _,_,_,_,_,_, 0,1,1,1,1,1],


    [_,0,0,_,_,1, 0,1,1,0,0,_, _,0,0,_,_,1],
    [_,_,1,_,_,1, _,_,0,1,1,0, 0,0,1,_,_,1],

    [_,_,1,_,_,1, _,_,_,_,_,_, 0,1,1,0,0,1],
    [_,_,0,_,_,1, 1,0,0,1,1,_, _,1,0,_,_,_],

    [_,_,0,1,1,0, 0,1,1,0,0,_, _,_,0,_,_,_],
    [_,_,_,_,_,_, _,1,0,0,1,1, 1,1,_,_,_,_]
]).

petice(List, List_petice) :-
    append([[],L0,[_,_,_,_]], List),
    append([[_],L1,[_,_,_]], List),
    append([[_,_],L2,[_,_]], List),
    append([[_,_,_],L3,[_]], List),
    append([[_,_,_,_],L4,[]], List),
    transpose([L0,L1,L2,L3,L4],List_petice).

petice_all(Lists, Petice) :-
    maplist(petice, Lists, Seznam_seznamu_petic),
    append(Seznam_seznamu_petic, Petice).

not_los_if_not_ground(Petice) :-
    ground(Petice)
    -> true
    ; dif(Petice, [0,1,1,0,0]), dif(Petice, [0,1,1,1,1]), dif(Petice, [1,0,0,1,1]).

dec_row([], [], []).
dec_row([B00, B01|B0_rest], [B10, B11|B1_rest], [Dec_digit|Dec_digit_rest]) :-
    Dec_digit #= 8 * B00 + 4 * B01 + 2 * B10 + B11,
    dec_row(B0_rest, B1_rest, Dec_digit_rest)
    .

dec_rows([],[]).
dec_rows([Row0, Row1|Rest], [Row_dec|Row_dec_rest]) :-
    dec_row(Row0, Row1, Row_dec),
    dec_rows(Rest, Row_dec_rest)
    .

sudoku(Rows) :-
        length(Rows, 9), maplist(same_length(Rows), Rows),
        append(Rows, Vs), Vs ins 1..9,
        maplist(all_distinct, Rows),
        transpose(Rows, Columns), maplist(all_distinct, Columns),
        Rows = [As,Bs,Cs,Ds,Es,Fs,Gs,Hs,Is],
        blocks(As, Bs, Cs), blocks(Ds, Es, Fs), blocks(Gs, Hs, Is).

blocks([], [], []).
blocks([N1,N2,N3|Ns1], [N4,N5,N6|Ns2], [N7,N8,N9|Ns3]) :-
        all_distinct([N1,N2,N3,N4,N5,N6,N7,N8,N9]),
        blocks(Ns1, Ns2, Ns3).

solve(Dec_rows) :-
    zadani(Z),
    petice_all(Z, Petice_rows),
    transpose(Z, Z_tr),
    petice_all(Z_tr, Petice_cols),
    append(Petice_rows, Petice_cols, Petice_everything),
    maplist(not_los_if_not_ground, Petice_everything),
    append(Z, Bin_vars),
    Bin_vars ins 0..1,
    dec_rows(Z, Dec_rows),
    sudoku(Dec_rows),
    append(Dec_rows, Dec_vars),
    label(Dec_vars)
    .

% V praxi se ukazuje, ze bez te LOS podminky ma sudoku 4 reseni, takze
% by mozna stacilo to spravne pak nejak vybrat rucne pomoci editoru a
% pocitani poctu vyskytu pomoci search v editoru.
