% This code is based on the following code by Markus Triska:
% https://swish.swi-prolog.org/example/clpfd_sudoku.pl

% This program is not just a puzzle solver but also a puzzle
% generator that takes the solution as input and generates puzzle
% instances.
% The puzzle that inspired this program is available at
% https://www.potrati.cz/2019/sifry/12-zabijak
% It appeared as part of the outdoor night puzzlehunt Po trati 2019.
%
% The puzzle is essentially a 12x12 sudoku with the property that
% the pattern of divisibility by three in a 12x6 table obtained
% by pairwise summation reads as a certain sensible text in Braille.

% tested with SWI Prolog

:- use_module(library(clpfd)).

find_puzzle(Rows) :-
        problem(P),
        sudoku(P, Rows),
        append(Rows, Vars),
        label(Vars).

sudoku(Problem, Rows) :-
        length(Problem, 12), maplist(same_length([a,a,a,a,a,a]), Problem),
        append(Problem, Problem_variables), Problem_variables ins 0..1,
        length(Rows, 12), maplist(same_length(Rows), Rows),
        append(Rows, Vs), Vs ins 1..12,
        maplist(all_distinct, Rows),
        transpose(Rows, Columns),
        maplist(all_distinct, Columns),
        Rows = [A,B,C,D,E,F,G,H,I,J,K,L],
        Problem = [A_,B_,C_,D_,E_,F_,G_,H_,I_,J_,K_,L_],
        blocks(A, B, C, A_, B_, C_), blocks(D, E, F, D_,E_,F_),
	blocks(G, H, I, G_,H_,I_), blocks(J, K, L, J_,K_,L_).

blocks([], [], [], [], [], []).
blocks(
    [A,B,C,D|Bs1], [E,F,G,H|Bs2], [I,J,K,L|Bs3],
    [A_,C_|Bs1_], [E_,G_|Bs2_], [I_,K_|Bs3_]
) :-
        all_distinct([A,B,C,D,E,F,G,H,I,J,K,L]),
        helper(A,B,A_),
        helper(C,D,C_),
        helper(E,F,E_),
        helper(G,H,G_),
        helper(I,J,I_),
        helper(K,L,K_),
        blocks(Bs1, Bs2, Bs3, Bs1_, Bs2_, Bs3_).

helper(A,B,A_) :-
    AB_div3 #>= 0, A_res in 1..2,
    (   A + B) #= (3 * AB_div3) + (A_ * A_res).

% KOSTANTONINA in (inverted) Braille (the text made sense in-game)
problem([
        [0,1, 0,1, 1,0],
        [1,1, 1,0, 0,1],
        [0,1, 0,1, 0,1],

        [1,0, 0,1, 0,0],
        [0,0, 1,1, 1,0],
        [0,1, 1,1, 0,1],

        [1,0, 0,1, 0,0],
        [0,0, 1,0, 1,0],
        [0,1, 0,1, 0,1],

        [1,0, 0,0, 0,1],
        [0,1, 1,0, 1,1],
        [1,1, 0,1, 1,1]
]).






