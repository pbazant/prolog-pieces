% This program solves the puzzle "P3 Súčtové pyramídy"
% that was part of the "InterLos 2021" online puzzle solving competition.
% The puzzle is available at
% https://interlos.fi.muni.cz/download/years/2021/sada1/sada1.pdf

% tested under SWI Prolog

:- table count_solutions/3.

count_solutions([], 0, 1).
count_solutions([Weight|Weights_rest], Sum, N) :-
    findall(
        PartN,
        (   between(0, Sum, SumLeft),
            SumRight is Sum - SumLeft,
            0 is SumLeft mod Weight,
            count_solutions(Weights_rest, SumRight, PartN)
        ),
        PartNs
    ),
    sumlist(PartNs, N).
