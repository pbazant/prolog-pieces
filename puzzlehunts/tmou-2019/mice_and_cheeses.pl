﻿% This program solves the tile (piece) placement step in the puzzle
% codenamed "2🐭 DEGUSTACE" that was part of the "Tmou 2019" outdoor
% night puzzlehunt. The paper component of the puzzle is available at
% https://www.tmou.cz/storage/21/2_degustace.pdf The puzzle also
% contained a physical set of 6 different cheeses, the identification of
% which was a necessary step in the full solution.

% tested under SWI Prolog

desc_piece(diag-diag, [A-C,B-D,A-C,B-D]).
desc_piece(diag-antidiag, [A-C,B-C,A-D,B-D]).
desc_piece(antidiag-diag, [A-C,A-D,B-C,B-D]).
desc_piece(antidiag-antidiag, [A-C,A-C,B-D,B-D]).

pieces_as_descs([
    diag-diag,
    diag-diag,
    diag-diag,
    diag-antidiag,
    diag-antidiag,
    antidiag-diag,
    antidiag-diag,
    antidiag-diag,
    antidiag-antidiag
]).

topology(
    [C,D,G,R,W,X],
    [
        [A,D,E,H],
        [B,E,F,I],
        [C,F,G,J],
        [H,K,L,O],
        [I,L,M,P],
        [J,M,N,Q],
        [O,R,S,V],
        [P,S,T,W],
        [Q,T,U,X]
    ]
) :- A=N, B=K, U=V.


boundary([
    emental-hermelin,
    mozzarella-niva,
    korbacek-olomoucky,
    mozzarella-olomoucky,
    emental-hermelin,
    korbacek-niva
]).

solve(Piece_descs_permuted) :-
    pieces_as_descs(Piece_descs),
    topology(
        Boundary,
        Placement
    ),
    boundary(Boundary),
    maplist(desc_piece, Piece_descs_permuted, Placement),
    permutation(Piece_descs, Piece_descs_permuted).

% setof(P, solve(P), Ps), write(Ps).
