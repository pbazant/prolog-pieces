% This is an implementation of the Luhn algorithm using CLP/FD in
% Prolog.
% I originally posted it here as a response to a coding
% challenge:
% https://www.reddit.com/r/prolog/comments/f95rm2/weekly_coding_challenge_4_luhn_algorithm/fjay7wo?utm_source=share&utm_medium=web2x&context=3
% The solution speculatively computes both sums (for even as well
% as for odd list lengths) and then uses the correct one. No cuts, works
% in all directions, no dangling choice-points.

:- use_module(library(clpfd)).

luhn2(L) :- luhn2(L, 0, 0), label(L).

luhn2([], 0, _).
luhn2([Digit|Rest], Sum_if_even, Sum_if_odd) :-
    Digit in 0..9,
    Sum_rest_if_even #= (Digit + Sum_if_odd) mod 10,
    Transformed #= 2 * Digit div 10 + 2 * Digit mod 10,
    Sum_rest_if_odd #= ( Transformed + Sum_if_even) mod 10,
    luhn2(Rest, Sum_rest_if_even, Sum_rest_if_odd).

% Example usage:

% ?- luhn2([5,2,0,4,4,0,8,0,8,6,5,6,6,4,9,2]).
% true.
% ?- luhn2([0,0,0,5,2,0,4,4,0,8,0,8,6,5,6,6,4,9,2]).
% true.
% ?- luhn2([5,2,0,4,4,0,8,0,8,6,5,6,6,4,9,D]).
% D = 2.
% ?- luhn2(L).
% L = []
% L = [0]
% L = [0, 0]
% L = [1, 8]
% L = [2, 6]
% ...
