% The predicate dist/3 calculates the least number of moves necessary
% to transform a Tower of Hanoi from the given initial state to
% the given final state. Inspired by puzzle number 10
% (http://bedna.org/2021/sifry/10) at the Bedna 2021 puzzlehunt.

m([A|Ar], B, Ar, [A|B]) :- maplist(<(A), B).
m2(I1, I2, O1, O2) :- m(I1, I2, O1, O2).
m2(I1, I2, O1, O2) :- m(I2, I1, O2, O1).
move(I1-I2-A, O1-O2-A) :- m2(I1, I2, O1, O2).
move(I1-A-I2, O1-A-O2) :- m2(I1, I2, O1, O2).
move(A-I1-I2, A-O1-O2) :- m2(I1, I2, O1, O2).

extend_set(Set, Boundary, Extended_set, Extended_boundary) :-
    setof(
        Position_next,
        Position^Boundary^(member(Position, Boundary),
                           move(Position, Position_next)
                          ),
        Extended_boundary_candidates
    ),
    ord_subtract(Extended_boundary_candidates, Set, Extended_boundary),
    ord_union(Set, Extended_boundary, Extended_set).

d(_Visited, Boundary, Final, 0) :- member(Final, Boundary).
d(Visited, Boundary, Final, Distance) :-
    maplist(dif(Final), Boundary),
    extend_set(Visited, Boundary, Visited_next, Boundary_next),
    d(Visited_next, Boundary_next, Final, Distance2),
    Distance is Distance2 + 1.

dist(Initial, Final, D) :-
    d([Initial], [Initial], Final, D).
