:- use_module(library(clpfd)).

% inspired by https://positiveanalogue.com/blog/puzzle/
% (mentioned here: https://twitter.com/acutmore/status/1368481500744794112)
% This predicate solves a triangular https://en.wikipedia.org/wiki/Peg_solitaire
% of size Size.

solve(Size, States) :-
    setof(
        [P1, P2, P3]-p,
        (
            [P1, P2, P3] ins 0..sup,
            P1 + P2 + P3 #= Size - 1,
            label([P1, P2, P3])
        ),
        State_0
    ),
    select(Position-p, State_0, Position-'.', State_1),
    same_length(State_0, [_|States]),
    states(State_1, States).

states(State, [State]).
states(State, [State | States_rest]) :-
    select(Position_from-p, State, Position_from-'.', State_1),
    select(Position_over-p, State_1, Position_over-'.', State_2),
    maplist(plus, [D0, D1, D2], Position_from, Position_over),
    2 is abs(D0) + abs(D1) + abs(D2),
    select(Position_to-'.', State_2, Position_to-p, State_next),
    maplist(plus, [D0, D1, D2], Position_over, Position_to),
    states(State_next, States_rest).

% printing stolen from https://positiveanalogue.com/blog/puzzle/
print_solution_5 :-
    solve(5, States),
    ignore((
        member([
            _-A, _-B, _-C, _-D, _-E,
            _-F, _-G, _-H, _-I,
            _-J, _-K, _-L,
            _-M, _-N,
            _-O
        ], States),
        format('    ~w~n', [O]),
        format('   ~w ~w~n', [M, N]),
        format('  ~w ~w ~w~n', [J, K, L]),
        format(' ~w ~w ~w ~w~n', [F, G, H, I]),
        format('~w ~w ~w ~w ~w\n\n', [A, B, C, D, E]),
        false
    )).
